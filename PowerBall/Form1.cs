﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PowerBall
{
    public partial class Form1 : Form
    {
        List<BetSite> listBetSite = new List<BetSite>();

        List<Thread> listThread = new List<Thread>();

        public Form1()
        {
            InitializeComponent();
            FormClosing += ExitEvent;
        }

        private void ExitEvent(object sender, FormClosingEventArgs e)
        {
            button_Save_Click(null,null);
        }

        public void PrintLog(string msg_ , bool betLog_ = false)
        {
            listBox_Log.Invoke(new MethodInvoker(delegate() {
                Util.AutoLineStringAdd(listBox_Log , msg_);
            }));

            if (betLog_)
                Global.listBetLog.Add(DateTime.Now.ToString("HH:mm:ss | ") + msg_);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
#if TEST
            Util.KillProcess("chromedriver");
#else
            Util.KillProcess("chrome");
#endif
            Global.uiController = this;
            

            EventUtil.ConnectCustomEvent(this);
            
            button_Stop.Enabled = false;
            button_Start.Enabled = false;

            MakeSiteList();

            dataGridView_SiteInfo.DataSource = Global.listSiteInfo;
            dataGridView_SiteInfo.Update();
        }

        void MakeSiteList()
        {
            SiteInfo site = new SiteInfo();
            site.사용 = false;
            site.사이트 = "파워사다리";
            site.보유머니 = "";
            site.PickPage = "http://picksite.co.kr/pick/oddeven/analysis.php?sort=ps";
            Global.listSiteInfo.Add(site);

            SiteInfo site2 = new SiteInfo();
            site2.사용 = false;
            site2.사이트 = "스피드키노";
            site2.보유머니 = "";
            site2.PickPage = "http://picksite.co.kr/pick/oddeven/analysis.php?sort=sk";
            Global.listSiteInfo.Add(site2);

            //             SiteInfo site3 = new SiteInfo();
            //             site3.사용 = true;
            //             site3.사이트 = "키노사다리";
            //             site3.보유머니 = "";
            //             site3.PickPage = "http://picksite.co.kr/pick/oddeven/analysis.php?sort=ks";
            //             Global.listSiteInfo.Add(site3);

            SiteInfo site4 = new SiteInfo();
            site4.사용 = false;
            site4.사이트 = "파워볼";
            site4.보유머니 = "";
            site4.PickPage = "http://picksite.co.kr/pick/oddeven/analysis.php?sort=pbp";
            Global.listSiteInfo.Add(site4);

#if TEST
            PrintLog("스피드키노와 키노사다리를 같이 사용하려면 서로 다른 계정을 사용해야 합니다.");
#endif
        }

        private void button_Ready_Click(object sender, EventArgs e)
        {
            listThread.Clear();

            foreach (var item in Global.listBetSite)
                item.QuiteDriver();
            Global.listBetSite.Clear();

            foreach (var item in Global.listSiteInfo)
            {
                if (item.사용 == false)
                    continue;

                Type type = Type.GetType(item.사이트);
                BetSite betSite = (BetSite)Activator.CreateInstance(type);
                Global.listBetSite.Add(betSite);  
            }

            button_Start.Enabled = true;
            button_Ready.Enabled = false;

            dataGridView_SiteInfo.Enabled = false;
        }

        private void button_Start_Click(object sender, EventArgs e)
        {
            PrintLog("시작");
            button_Start.Enabled = false;
            button_Stop.Enabled = true;
            Global.stop = false;
            groupBox_Setting.Enabled = false;

            foreach(var item in Global.listBetSite)
            {
                Thread worker = new Thread(()=> WorkThread(item , Global.listSiteInfo.Find(x=>x.사이트 == item.GetType().ToString())));
                worker.Start();
                listThread.Add(worker);
            }

            Thread endCheck = new Thread(EndCheckThread);
            endCheck.Start();
        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            PrintLog("정지중...");
            Global.stop = true;
            button_Stop.Enabled = false;
        }

        int HavetoWait(int remain_)
        {
            if (Global.numericUpDown_RemainMin <= remain_ && Global.numericUpDown_RemainMax >= remain_)
            {
                return 0;
            }               
            else
            {
                if (Global.numericUpDown_RemainMin > remain_)
                    return -1;
                else
                    return remain_ - Global.numericUpDown_RemainMax +1;
            }
        }

        string PargetBetTarget(string data_)
        {
            var match = Regex.Matches(data_, @"([-+]?[0-9]*\.?[0-9]+)");
            if (match.Count == 2)
            {
                float oddValue = float.Parse(match[0].Value);
                float notValue = float.Parse(match[1].Value);
                if (oddValue > notValue)
                    return "홀";
                else
                    return "짝";
            }
            else if (match.Count == 1)
            {
                if (data_.Contains("홀"))
                    return "홀";
                else
                    return "짝";
            }
            else
            {
                return "";
            }                           
        }

        void DoneToWork()
        {
            listThread.Clear();
            this.Invoke(new MethodInvoker(delegate () {
                groupBox_Setting.Enabled = true;
                button_Ready.Enabled = true;
                dataGridView_SiteInfo.Enabled = true;
            }));          
        }

        void UpdatePickPointLabel(int value_ = -1)
        {
            label_Point.Invoke(new MethodInvoker(delegate () {
                 label_Point.Text = value_.ToString();
            }));
        }

        void UpdateGridDataUI()
        {
            dataGridView_SiteInfo.Invoke(new MethodInvoker(delegate () {
                dataGridView_SiteInfo.Refresh();
            }));
        }

        void EndCheckThread()
        {
            while(true)
            {
                bool bAllEnd = true;
                foreach (Thread item in listThread)
                {
                    if (item.IsAlive)
                    {
                        bAllEnd = false;
                        break;
                    }
                }
                if (bAllEnd)
                    break;
                else
                    Thread.Sleep(1000);
            }
            DoneToWork();
            PrintLog("모든 작업 정지 완료");
        }
    
        void WorkThread(BetSite betSite_ , SiteInfo siteInfo_)
        {
            PickSite pickSite = new PickSite();
            while (true)
            {
                if (Global.stop)
                    break;
#if !TEST
                try
#endif
                {
                    if (betSite_.CheckUnusageAlram())
                    {
                        PrintLog(siteInfo_.사이트 + " : 장기간 미사용 알람 확인");
                        Thread.Sleep(1000 * 3);
                    }

                    int moneyInfo = betSite_.GetMoneyInfo();
                    siteInfo_.보유머니 = moneyInfo.ToString();
                    UpdateGridDataUI();
                    PrintLog(siteInfo_.사이트 + " : 남은 금액 : " + moneyInfo);

                    if (moneyInfo < Global.numericUpDown_BetMoney)
                    {
                        PrintLog(siteInfo_.사이트 + " : 보유금액 부족으로 정지");
                        button_Stop.PerformClick();
                        //button_Stop_Click(null, null);
                        break;
                    }

                    PrintLog(siteInfo_.사이트 + " : 게임라운드 정보 파싱");
                    List<int> targetInfo = pickSite.TargetPlayInfo(siteInfo_.PickPage);
                    if (targetInfo.Count <= 1)
                    {
                        PrintLog(siteInfo_.사이트 + " : 게임라운드 정보 파싱 대기!!");
                        int sleepSec = 10;
                        Thread.Sleep(1000 * sleepSec);
                        PrintLog(siteInfo_.사이트 + " : " + sleepSec + "초 대기");
                        continue;
                    }
                    else
                    {
                        if (pickSite.IsWainting(siteInfo_.PickPage))
                        {
                            PrintLog(siteInfo_.사이트 + " : "+targetInfo[0] + "회 " + targetInfo[1] + "초 대기");
                            Thread.Sleep(1000 * targetInfo[1]);
                        }
                        else
                        {
                            if (pickSite.IsPosibleGet(siteInfo_.PickPage))
                            {
                                int remain = HavetoWait(targetInfo[1]);
                                if (remain >= 0)
                                {
                                    PrintLog(siteInfo_.사이트 +  " : " + remain + "초 대기");
                                    Thread.Sleep(1000 * remain);

                                    PrintLog(siteInfo_.사이트 + " : " + targetInfo[0] + "회 예상 보기");
                                    string ret = pickSite.GetExpertInfo(siteInfo_.PickPage);
                                    //string ret = "홀53.12%";
                                    PrintLog(siteInfo_.사이트 + " : " + ret);

                                    int remainCoin = pickSite.GetCoinInfo();
                                    UpdatePickPointLabel(remainCoin);
                                    if (remainCoin < 2)
                                    {
                                        PrintLog(siteInfo_.사이트 + " : " + targetInfo[0] + "회 코인부족이거나 픽업 사이트 로그아웃으로 정지 진행");
                                        button_Stop_Click(null, null);
                                        break;
                                    }

                                    string target = PargetBetTarget(ret);
                                    if (string.IsNullOrEmpty(target))
                                    {
                                        PrintLog(siteInfo_.사이트 + " : 예상 결과 파싱 대기!!!");
                                        PrintLog(siteInfo_.사이트 + " : " + targetInfo[0] + "회 " + targetInfo[1] + "초 대기");
                                        Thread.Sleep(1000 * targetInfo[1]);
                                    }
                                    else
                                    {
                                        GameInfo info = new GameInfo();
                                        info.회 = targetInfo[0];
                                        info.실제 = "";
                                        Global.listGameInfo.Add(info);
                                        info.예상 = target;

                                        PrintLog(siteInfo_.사이트 + " : " + targetInfo[0] + "회 " + target + " -> " + Global.numericUpDown_BetMoney + "배팅" , true);
                                        betSite_.DoBet(target, Global.numericUpDown_BetMoney);
                                        PrintLog(siteInfo_.사이트 + " : 다음배팅 대기");
                                        Thread.Sleep(1000 * targetInfo[1]);


                                    }
                                }
                                else
                                {
                                    PrintLog(siteInfo_.사이트 + " :" + targetInfo[0] + "회 최소 남은시간을 이미 지났습니다.");
                                    PrintLog(siteInfo_.사이트 + " : " + targetInfo[0] + "회 " + targetInfo[1] + "초 대기");
                                    Thread.Sleep(1000 * targetInfo[1]);
                                }
                            }
                            else
                            {
                                PrintLog(siteInfo_.사이트 + " : 보기 버튼 노출 대기");
                                Thread.Sleep(1000 * 5);
                            }
                        }
                    }

                    Thread.Sleep(1000*3);
                }
#if !TEST
                catch
                {
                    PrintLog(siteInfo_.사이트 + " : 예외처리!!");
                }
#endif
            }

            betSite_.QuiteDriver();
            pickSite.QuiteDriver();

            PrintLog(siteInfo_.사이트 + " : 정지 완료");
        }

        private void button_Save_Click(object sender, EventArgs e)
        {
            string fileName = "BetLog_" + DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss");
            PrintLog("배팅정보 저장 -> " + fileName);

            Util.SaveTxtFile(fileName, Global.listBetLog, "Log");

        }

        
    }
}
