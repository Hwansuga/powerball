﻿namespace PowerBall
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_Stop = new System.Windows.Forms.Button();
            this.numericUpDown_RemainMin = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_RemainMax = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_BetMoney = new System.Windows.Forms.NumericUpDown();
            this.checkBox_Real = new System.Windows.Forms.CheckBox();
            this.button_Save = new System.Windows.Forms.Button();
            this.groupBox_CollectDB = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label2 = new System.Windows.Forms.Label();
            this.label_Point = new System.Windows.Forms.Label();
            this.groupBox_Setting = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.button_Ready = new System.Windows.Forms.Button();
            this.dataGridView_SiteInfo = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RemainMin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RemainMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BetMoney)).BeginInit();
            this.groupBox_CollectDB.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox_Setting.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SiteInfo)).BeginInit();
            this.SuspendLayout();
            // 
            // listBox_Log
            // 
            this.listBox_Log.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(291, 9);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(377, 376);
            this.listBox_Log.TabIndex = 0;
            // 
            // button_Start
            // 
            this.button_Start.Location = new System.Drawing.Point(6, 56);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(130, 25);
            this.button_Start.TabIndex = 1;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_Stop
            // 
            this.button_Stop.Location = new System.Drawing.Point(142, 56);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(125, 25);
            this.button_Stop.TabIndex = 2;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // numericUpDown_RemainMin
            // 
            this.numericUpDown_RemainMin.Location = new System.Drawing.Point(1, 1);
            this.numericUpDown_RemainMin.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_RemainMin.Maximum = new decimal(new int[] {
            180,
            0,
            0,
            0});
            this.numericUpDown_RemainMin.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown_RemainMin.Name = "numericUpDown_RemainMin";
            this.numericUpDown_RemainMin.Size = new System.Drawing.Size(52, 21);
            this.numericUpDown_RemainMin.TabIndex = 3;
            this.numericUpDown_RemainMin.Value = new decimal(new int[] {
            60,
            0,
            0,
            0});
            // 
            // numericUpDown_RemainMax
            // 
            this.numericUpDown_RemainMax.Location = new System.Drawing.Point(86, 1);
            this.numericUpDown_RemainMax.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_RemainMax.Maximum = new decimal(new int[] {
            240,
            0,
            0,
            0});
            this.numericUpDown_RemainMax.Minimum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown_RemainMax.Name = "numericUpDown_RemainMax";
            this.numericUpDown_RemainMax.Size = new System.Drawing.Size(55, 21);
            this.numericUpDown_RemainMax.TabIndex = 4;
            this.numericUpDown_RemainMax.Value = new decimal(new int[] {
            240,
            0,
            0,
            0});
            // 
            // numericUpDown_BetMoney
            // 
            this.numericUpDown_BetMoney.Increment = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown_BetMoney.Location = new System.Drawing.Point(116, 3);
            this.numericUpDown_BetMoney.Margin = new System.Windows.Forms.Padding(1);
            this.numericUpDown_BetMoney.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_BetMoney.Minimum = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            this.numericUpDown_BetMoney.Name = "numericUpDown_BetMoney";
            this.numericUpDown_BetMoney.Size = new System.Drawing.Size(142, 21);
            this.numericUpDown_BetMoney.TabIndex = 5;
            this.numericUpDown_BetMoney.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.numericUpDown_BetMoney.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // checkBox_Real
            // 
            this.checkBox_Real.AutoSize = true;
            this.checkBox_Real.Location = new System.Drawing.Point(6, 78);
            this.checkBox_Real.Name = "checkBox_Real";
            this.checkBox_Real.Size = new System.Drawing.Size(49, 16);
            this.checkBox_Real.TabIndex = 6;
            this.checkBox_Real.Text = "Real";
            this.checkBox_Real.UseVisualStyleBackColor = true;
            // 
            // button_Save
            // 
            this.button_Save.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_Save.Location = new System.Drawing.Point(291, 401);
            this.button_Save.Name = "button_Save";
            this.button_Save.Size = new System.Drawing.Size(377, 23);
            this.button_Save.TabIndex = 7;
            this.button_Save.Text = "배팅로그 저장";
            this.button_Save.UseVisualStyleBackColor = true;
            this.button_Save.Click += new System.EventHandler(this.button_Save_Click);
            // 
            // groupBox_CollectDB
            // 
            this.groupBox_CollectDB.Controls.Add(this.tableLayoutPanel2);
            this.groupBox_CollectDB.Controls.Add(this.button_Start);
            this.groupBox_CollectDB.Controls.Add(this.button_Stop);
            this.groupBox_CollectDB.Location = new System.Drawing.Point(12, 219);
            this.groupBox_CollectDB.Name = "groupBox_CollectDB";
            this.groupBox_CollectDB.Size = new System.Drawing.Size(273, 92);
            this.groupBox_CollectDB.TabIndex = 8;
            this.groupBox_CollectDB.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.64103F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.35897F));
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label_Point, 1, 0);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(6, 20);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(261, 30);
            this.tableLayoutPanel2.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(3, 3);
            this.label2.Margin = new System.Windows.Forms.Padding(1);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label2.Size = new System.Drawing.Size(127, 24);
            this.label2.TabIndex = 0;
            this.label2.Text = "Pick Point";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label_Point
            // 
            this.label_Point.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Point.AutoSize = true;
            this.label_Point.Location = new System.Drawing.Point(134, 3);
            this.label_Point.Margin = new System.Windows.Forms.Padding(1);
            this.label_Point.Name = "label_Point";
            this.label_Point.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label_Point.Size = new System.Drawing.Size(124, 24);
            this.label_Point.TabIndex = 5;
            this.label_Point.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // groupBox_Setting
            // 
            this.groupBox_Setting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox_Setting.Controls.Add(this.tableLayoutPanel3);
            this.groupBox_Setting.Controls.Add(this.checkBox_Real);
            this.groupBox_Setting.Location = new System.Drawing.Point(12, 319);
            this.groupBox_Setting.Name = "groupBox_Setting";
            this.groupBox_Setting.Size = new System.Drawing.Size(273, 105);
            this.groupBox_Setting.TabIndex = 9;
            this.groupBox_Setting.TabStop = false;
            this.groupBox_Setting.Text = "설정";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel3.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.87352F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.12648F));
            this.tableLayoutPanel3.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.numericUpDown_BetMoney, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel1, 1, 1);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(6, 20);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(261, 52);
            this.tableLayoutPanel3.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(3, 3);
            this.label10.Margin = new System.Windows.Forms.Padding(1);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label10.Size = new System.Drawing.Size(109, 21);
            this.label10.TabIndex = 0;
            this.label10.Text = "배팅머니";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(3, 28);
            this.label11.Margin = new System.Windows.Forms.Padding(1);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.label11.Size = new System.Drawing.Size(109, 21);
            this.label11.TabIndex = 1;
            this.label11.Text = "Pick 보기 구간(초)";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 38.31169F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.07792F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.25974F));
            this.tableLayoutPanel1.Controls.Add(this.label1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_RemainMin, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_RemainMax, 2, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(116, 28);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(142, 21);
            this.tableLayoutPanel1.TabIndex = 6;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(55, 1);
            this.label1.Margin = new System.Windows.Forms.Padding(1);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(29, 19);
            this.label1.TabIndex = 10;
            this.label1.Text = "~";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_Ready
            // 
            this.button_Ready.Location = new System.Drawing.Point(12, 190);
            this.button_Ready.Name = "button_Ready";
            this.button_Ready.Size = new System.Drawing.Size(273, 23);
            this.button_Ready.TabIndex = 10;
            this.button_Ready.Text = "준비";
            this.button_Ready.UseVisualStyleBackColor = true;
            this.button_Ready.Click += new System.EventHandler(this.button_Ready_Click);
            // 
            // dataGridView_SiteInfo
            // 
            this.dataGridView_SiteInfo.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dataGridView_SiteInfo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_SiteInfo.Location = new System.Drawing.Point(12, 9);
            this.dataGridView_SiteInfo.Name = "dataGridView_SiteInfo";
            this.dataGridView_SiteInfo.RowHeadersVisible = false;
            this.dataGridView_SiteInfo.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.DisableResizing;
            this.dataGridView_SiteInfo.RowTemplate.Height = 23;
            this.dataGridView_SiteInfo.Size = new System.Drawing.Size(273, 175);
            this.dataGridView_SiteInfo.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(680, 430);
            this.Controls.Add(this.dataGridView_SiteInfo);
            this.Controls.Add(this.button_Ready);
            this.Controls.Add(this.groupBox_Setting);
            this.Controls.Add(this.groupBox_CollectDB);
            this.Controls.Add(this.button_Save);
            this.Controls.Add(this.listBox_Log);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "PowerBall";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RemainMin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_RemainMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_BetMoney)).EndInit();
            this.groupBox_CollectDB.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox_Setting.ResumeLayout(false);
            this.groupBox_Setting.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_SiteInfo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.NumericUpDown numericUpDown_RemainMin;
        private System.Windows.Forms.NumericUpDown numericUpDown_RemainMax;
        private System.Windows.Forms.NumericUpDown numericUpDown_BetMoney;
        private System.Windows.Forms.CheckBox checkBox_Real;
        private System.Windows.Forms.Button button_Save;
        private System.Windows.Forms.GroupBox groupBox_CollectDB;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Label label_Point;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox_Setting;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button button_Ready;
        private System.Windows.Forms.DataGridView dataGridView_SiteInfo;
    }
}

