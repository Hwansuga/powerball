﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using PowerBall;

public class GameInfo
{
    public int 회 { get; set; }
    public string 실제 { get; set; }
    public string 예상 { get; set; }
}

public class SiteInfo
{
    public bool 사용 { get; set; }
    public string 사이트 { get; set; }
    public string 보유머니 { get; set; }
    public string PickPage { get; set; }
}


class Global : Singleton<Global>
{
    public static bool stop = true;
    public static Form1 uiController = null;

    public static int numericUpDown_BetMoney = 5000;
    public static int numericUpDown_RemainMin = 60;
    public static int numericUpDown_RemainMax = 240;

    public static bool checkBox_Real = false;

    public static List<GameInfo> listGameInfo = new List<GameInfo>();

    public static List<SiteInfo> listSiteInfo = new List<SiteInfo>();
    public static List<BetSite> listBetSite = new List<BetSite>();

    public static List<string> listBetLog = new List<string>();



    public static void PrintLog(string msg_)
    {
        if (uiController == null)
            return;

        uiController.PrintLog(msg_);
    }
}

