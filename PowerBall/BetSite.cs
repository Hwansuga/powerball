﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class BetSite : DriverController
{
    public string loginUrl;
    public string targetUrl;
    public string id;
    public string pw;

    public void LoadAccInfo(string fileName_)
    {
        List<string> listInfo = new List<string>();
        Util.LoadTxtFile(fileName_, ref listInfo);
        if (listInfo.Count <= 0)
            return;

        string[] accInfo = listInfo[0].Split(',');
        id = accInfo[0];
        pw = accInfo[1];
    }

    public virtual void SetUrl() { }

    public virtual void Login() { }

    public virtual void SetBetMoney(int value_) { }

    public virtual void ClickOddNot(string value_) { }

    public virtual void DoBet(string target_, int betMoney_) { }

    public virtual int GetMoneyInfo() { return 0; }
    public virtual bool CheckUnusageAlram() { return false; }
}

