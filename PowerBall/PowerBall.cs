﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;


class 파워볼 : 파워사다리
{
    public override void SetUrl()
    {
        loginUrl = "http://www.tys-ball.com";

        targetUrl = "http://www.tys-ball.com/indiv_games/view/1";

        LoadAccInfo("AccBallSite.txt");

        Global.PrintLog("타이슨 로그인 후 파워볼로 이동 해주세요");
    }

    public override void ClickOddNot(string value_)
    {
        string attr = string.Format("*[data-pick-name='파워볼 {0}']", value_);
        IWebElement btn = driver.FindElementByCssSelector(attr);
        IWebElement pargetBtn = btn.FindElement(By.XPath(".."));

        string innerHtml = pargetBtn.GetAttribute("innerHTML");

        if (innerHtml.Contains("selected") == false)
            ClickElement(btn);
        Thread.Sleep(1000);
    }
}

