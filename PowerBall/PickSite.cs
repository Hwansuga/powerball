﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;


class PickSite : DriverController
{
    string loginUrl = "http://picksite.co.kr/bbs/login.php";

    string id;
    string pw;
   
    public PickSite()
    {
        LoadAccInfo();

        if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(pw))
        {
            Global.PrintLog("AccPickSite.txt 안에 계정 정보가 없습니다.");
            return;
        }

        CreatDriver(TYPE_DRIVER.TYPE_Chrome);
#if !TEST
        driver.Manage().Window.Position = new System.Drawing.Point(-10000, -10000);
#endif

        Login();
    }

    public void LoadAccInfo()
    {
        List<string> listInfo = new List<string>();
        Util.LoadTxtFile("AccPickSite.txt", ref listInfo);
        if (listInfo.Count <= 0)
            return;

        string[] accInfo = listInfo[0].Split(',');
        id = accInfo[0];
        pw = accInfo[1];
    }

    public void Login()
    {
        driver.Navigate().GoToUrl(loginUrl);
        Thread.Sleep(1000);

        driver.FindElementById("login_id").SendKeys(id);
        Thread.Sleep(500);
        driver.FindElementById("login_pw").SendKeys(pw + Keys.Enter);
        Thread.Sleep(500);

//         driver.Navigate().GoToUrl(powerUrl);
//         Thread.Sleep(1000);
    }

    private readonly object TargetPlayInfoLock = new object();
    public List<int> TargetPlayInfo(string url_)
    {
        //lock (TargetPlayInfoLock)
        {
            List<int> listRet = new List<int>();
            if (driver.Url.Contains(url_) == false)
            {
                driver.Navigate().GoToUrl(url_);
                Thread.Sleep(1000);
            }
                       
            IWebElement label = driver.FindElementById("rtime");
            if (label == null)
                return listRet;

            try
            {
                string gameNum = label.FindElement(By.ClassName("blue")).Text;
                listRet.Add(int.Parse(gameNum));
                string remain = label.FindElement(By.ClassName("red")).Text;
                DateTime dt = DateTime.ParseExact(remain, "mm분 ss초", CultureInfo.InvariantCulture);
                int sec = dt.Minute * 60 + dt.Second;
                listRet.Add(sec);
            }
            catch
            {

            }

            return listRet;
        }                     
    }

    public int GetCoinInfo()
    {
        IWebElement web = driver.FindElementById("ol_after_pt");
        if (web == null)
            return -1;

        IWebElement point = web.FindElement(By.XPath("./button/b/span"));
        //*[@id="ol_after_pt"]
        //*[@id="ol_after_pt"]/button/b/span
        if (point.Text == "비회원")
        {
            return -1;
        }
        return int.Parse(point.Text.Replace(",",""));
    }

    private readonly object IsWaintingLock = new object();
    public bool IsWainting(string url_)
    {
        //lock(IsWaintingLock)
        {
            if (driver.Url.Contains(url_) == false)
            {
                driver.Navigate().GoToUrl(url_);
                Thread.Sleep(1000);
            }

            return driver.PageSource.Contains("btn_wait");
        }             
    }

    private readonly object IsPosibleGetLock = new object();
    public bool IsPosibleGet(string url_)
    {
        //lock (IsWaintingLock)
        {
            if (driver.Url.Contains(url_) == false)
            {
                driver.Navigate().GoToUrl(url_);
                Thread.Sleep(1000);
            }
            return driver.PageSource.Contains("btn_get");
        }    
    }

    private readonly object GetExpertInfoLock = new object();
    public string GetExpertInfo(string url_)
    {       
        //lock (GetExpertInfoLock)
        {
            string data = "";
            if (driver.Url.Contains(url_) == false)
            {
                driver.Navigate().GoToUrl(url_);
                Thread.Sleep(1000);
            }

            IWebElement btn = driver.FindElementByClassName("btn_get");
            if (btn == null)
                return "";

            btn.Click();
            Thread.Sleep(1000);

            IsAlertPresent();
            Thread.Sleep(2000);
            //*[@id="oddeven_percent"]
            data = driver.FindElementById("oddeven_percent").Text;

            return data;
        }
        
    }
}

