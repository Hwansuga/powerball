﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

class 스피드키노 : BetSite
{
    public 스피드키노()
    {
        SetUrl();
       
        if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(pw))
        {
            Global.PrintLog("AccStartSite.txt 안에 계정 정보가 없습니다.");
            return;
        }

        CreatDriver(TYPE_DRIVER.TYPE_Chrome);
        driver.Manage().Window.Maximize();
#if !TEST
        //driver.Manage().Window.Position = new System.Drawing.Point(-10000, -10000);
#endif
        Login();
    }

    public override void SetUrl()
    {
        loginUrl = "http://st-923.com";
        targetUrl = "http://st-923.com/bet/game.html?game_type=speed_keno";

        LoadAccInfo("AccSpeedKino.txt");
    }

    public override void Login()
    {
        driver.Navigate().GoToUrl(loginUrl);
        Thread.Sleep(1000);

        driver.FindElementByName("IU_ID").SendKeys(id);
        Thread.Sleep(500);
        driver.FindElementByName("IU_PW").SendKeys(pw + Keys.Enter);
        Thread.Sleep(500);

        driver.Navigate().GoToUrl(targetUrl);
        Thread.Sleep(1000);
    }

    public override void SetBetMoney(int value_)
    {
        List<IWebElement> listBtn = driver.FindElementByClassName("btnss").FindElements(By.XPath("./input")).ToList();
        IWebElement resetBtn = listBtn.Find(x => x.GetAttribute("value").Contains("정정"));
        resetBtn.Click();
        Thread.Sleep(1000);

        IWebElement web = driver.FindElementById("BetAmount");
        web.SendKeys(value_.ToString());
        Thread.Sleep(1000);
    }

    public override void ClickOddNot(string value_)
    {
        List<IWebElement> listBtn = driver.FindElementsByClassName("ptype").ToList();
        IWebElement btn = listBtn.Find(x => x.Text.Contains(value_));

        IWebElement btnRoot = btn.FindElement(By.XPath(".."));
        string innerHtml = btnRoot.GetAttribute("class");
        if (innerHtml.Contains("over") == false)
            ClickElement(btnRoot);

        Thread.Sleep(1000);

    }

    public override void DoBet(string target_, int betMoney_)
    {
        SetBetMoney(betMoney_);
        ClickOddNot(target_);

        if (Global.checkBox_Real)
        {
            IWebElement doBetBtn = driver.FindElementByClassName("betting");
            doBetBtn.Click();
            Thread.Sleep(1000);

            //really do bet?
            string msg = IsAlertPresent();
            Global.PrintLog(msg);
            Thread.Sleep(1000);

            //confirm
            msg = IsAlertPresent();
            Global.PrintLog(msg);
            Thread.Sleep(1000);
        }
    }

    public override int GetMoneyInfo()
    {
        IWebElement web = driver.FindElementById("user_cash");
        string money = web.Text;
        money = money.Replace(",", "").Replace("원", "");

        return int.Parse(money);
    }
}

