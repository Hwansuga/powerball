﻿using OpenQA.Selenium;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;


class 파워사다리 : BetSite
{
    public 파워사다리()
    {
        SetUrl();

        if (string.IsNullOrEmpty(id) || string.IsNullOrEmpty(pw))
        {
            Global.PrintLog("AccBallSite.txt 안에 계정 정보가 없습니다.");
            return;
        }

        CreatDriver(TYPE_DRIVER.TYPE_Chrome);
        driver.Manage().Window.Maximize();
               
        Login();     
    }

    public override void SetUrl()
    {
        loginUrl = "http://www.tys-ball.com";

        targetUrl = "http://www.tys-ball.com/indiv_games/view/7";

        LoadAccInfo("AccBallSite.txt");

        Global.PrintLog("타이슨 로그인 후 파워사다리로 이동 해주세요");
    }

    public override void Login()
    {
        driver.Navigate().GoToUrl(loginUrl);
        Thread.Sleep(1000);

        driver.FindElementById("UserUsername").SendKeys(id);
        Thread.Sleep(500);
        driver.FindElementById("UserPassword").SendKeys(pw);
        Thread.Sleep(500);
    }

    public void MoveBetPage()
    {
        driver.Navigate().GoToUrl(targetUrl);
        Thread.Sleep(2000);

        Global.PrintLog("사다리 페이지 갱신");
    }

    public override void SetBetMoney(int value_)
    {    
        List<IWebElement> listBtn = driver.FindElementByXPath("/html/body/div[4]/div/div[2]/div[3]/ul").FindElements(By.XPath("./li")).ToList();
        IWebElement resetBtn = listBtn.Find(x => x.Text.Contains("취소"));
        resetBtn.Click();
        Thread.Sleep(1000);

        IWebElement web = driver.FindElementByXPath("/html/body/div[4]/div/div[2]/div[2]/input");
        web.SendKeys(value_.ToString());
        Thread.Sleep(1000);
    }

    public override void ClickOddNot(string value_)
    {
        string attr = string.Format("*[data-pick-name='{0}']", value_);
        IWebElement btn = driver.FindElementByCssSelector(attr);

         IWebElement pargetBtn = btn.FindElement(By.XPath(".."));

        string innerHtml = pargetBtn.GetAttribute("innerHTML");

        if (innerHtml.Contains("selected") == false)
            ClickElement(btn);
        Thread.Sleep(1000);
    }

    public override void DoBet(string target_, int betMoney_)
    {
        SetBetMoney(betMoney_);
        ClickOddNot(target_);

        if (Global.checkBox_Real)
        {
            IWebElement doBetBtn = driver.FindElementByXPath("/html/body/div[4]/div/div[2]/div[3]/button");
            doBetBtn.Click();
            Thread.Sleep(1000);

            //really do bet?
            //List<IWebElement> listBtn = driver.FindElements(By.ClassName("l-btn-left")).ToList();
            //IWebElement reallyBtn = listBtn.Find(x => x.Text.Contains("확인")).FindElement(By.XPath(".."));

            IWebElement reallyBtn = driver.FindElementByXPath("//*[@id='confirmModal']/div/div/div[3]/button[2]");
            reallyBtn.Click();
            Thread.Sleep(2000);

            //confirm
            //listBtn = driver.FindElements(By.ClassName("l-btn-left")).ToList();
            //IWebElement okBtn = listBtn.Find(x => x.Text.Contains("확인")).FindElement(By.XPath("..")); ;

            try
            {
                IWebElement okBtn = driver.FindElementByXPath("//*[@id='confirmModal']/div/div/div[3]/button[1]");
                okBtn.Click();
                Thread.Sleep(1000);
            }
            catch
            {
                MoveBetPage();
            }
            
        }
    }

    public override int GetMoneyInfo()
    {
        IWebElement web = driver.FindElementById("user-now-balance");
        string money = web.Text;
        money = money.Replace(",", "").Replace("원", "");

        return int.Parse(money);
    }

    public override bool CheckUnusageAlram()
    {
        const string keyWrod = "미사용";
        if (Regex.Matches(driver.PageSource, keyWrod).Count > 1)
        {
            IWebElement okBtn = driver.FindElementByXPath("//*[@id='confirmModal']/div/div/div[3]/button[1]");
            okBtn.Click();
            Thread.Sleep(1000);
            return true;
        }
        else
        {
            return false;
        }           
    }
}

