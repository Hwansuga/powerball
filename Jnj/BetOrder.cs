﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Jnj
{
    public partial class BetOrder : UserControl
    {
        List<string> saveVar = null;

        List<ComboBox> listComboBox = new List<ComboBox>();

        public BetOrder()
        {
            InitializeComponent();
        }

        private void BetOrder_Load(object sender, EventArgs e)
        {
            List<FieldInfo> listField = this.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            List<FieldInfo> listCombobox = listField.FindAll(x => x.FieldType == typeof(ComboBox));
            Util.SortByNameNymber(listCombobox);
            foreach (FieldInfo item in listCombobox)
            {
                ComboBox temp = (ComboBox)item.GetValue(this);
                temp.SelectedIndexChanged += comboBox_Order_SelectedIndexChanged;

                listComboBox.Add(temp);
            }
        }

        public void ConnectVariable(ref List<string> order_)
        {
            this.saveVar = order_;

            this.UpdateUI();
        }

        public void UpdateUI()
        {
            if (this.saveVar == null)
                return;

            for(int i=0; i< this.saveVar.Count; ++i)
            {
                if (i < listComboBox.Count)
                {
                    listComboBox[i].SelectedItem = this.saveVar[i];
                }
            }
        }

        private void comboBox_Order_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.saveVar == null)
                return;

            ComboBox temp = (ComboBox)sender;
            int i = int.Parse(temp.Name.Replace("comboBox_Order", ""));
            this.saveVar[i] = temp.SelectedItem.ToString();
        }
    }
}
