﻿namespace Jnj
{
    partial class MoneyOrder
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.numericUpDown_Money_0 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_1 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.numericUpDown_Money_3 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_4 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_5 = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numericUpDown_Money_6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_8 = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.numericUpDown_Money_10 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_11 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_9 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_14 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_13 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_Money_12 = new System.Windows.Forms.NumericUpDown();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_0)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_11)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_9)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_14)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_13)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_12)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Inset;
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_12, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_13, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_14, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_8, 2, 5);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_7, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_6, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.label9, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.label8, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_5, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_4, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label6, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.label5, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_2, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_1, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_0, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label11, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.label12, 2, 6);
            this.tableLayoutPanel1.Controls.Add(this.label10, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.label13, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.label14, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.label15, 2, 8);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_9, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_10, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown_Money_11, 2, 7);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 10;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(272, 283);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(5, 2);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Step 1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.Location = new System.Drawing.Point(94, 2);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(82, 26);
            this.label2.TabIndex = 1;
            this.label2.Text = "Step 2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.Location = new System.Drawing.Point(184, 2);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(83, 26);
            this.label3.TabIndex = 2;
            this.label3.Text = "Step 3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_Money_0
            // 
            this.numericUpDown_Money_0.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_0.Location = new System.Drawing.Point(5, 33);
            this.numericUpDown_Money_0.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_0.Name = "numericUpDown_Money_0";
            this.numericUpDown_Money_0.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_0.TabIndex = 3;
            // 
            // numericUpDown_Money_1
            // 
            this.numericUpDown_Money_1.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_1.Location = new System.Drawing.Point(94, 33);
            this.numericUpDown_Money_1.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_1.Name = "numericUpDown_Money_1";
            this.numericUpDown_Money_1.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_1.TabIndex = 4;
            // 
            // numericUpDown_Money_2
            // 
            this.numericUpDown_Money_2.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_2.Location = new System.Drawing.Point(184, 33);
            this.numericUpDown_Money_2.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_2.Name = "numericUpDown_Money_2";
            this.numericUpDown_Money_2.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_2.TabIndex = 5;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.Location = new System.Drawing.Point(5, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 26);
            this.label4.TabIndex = 6;
            this.label4.Text = "Step 4";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label5.Location = new System.Drawing.Point(94, 58);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(82, 26);
            this.label5.TabIndex = 7;
            this.label5.Text = "Step 5";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label6.Location = new System.Drawing.Point(184, 58);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(83, 26);
            this.label6.TabIndex = 8;
            this.label6.Text = "Step 6";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_Money_3
            // 
            this.numericUpDown_Money_3.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_3.Location = new System.Drawing.Point(5, 89);
            this.numericUpDown_Money_3.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_3.Name = "numericUpDown_Money_3";
            this.numericUpDown_Money_3.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_3.TabIndex = 9;
            // 
            // numericUpDown_Money_4
            // 
            this.numericUpDown_Money_4.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_4.Location = new System.Drawing.Point(94, 89);
            this.numericUpDown_Money_4.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_4.Name = "numericUpDown_Money_4";
            this.numericUpDown_Money_4.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_4.TabIndex = 10;
            // 
            // numericUpDown_Money_5
            // 
            this.numericUpDown_Money_5.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_5.Location = new System.Drawing.Point(184, 89);
            this.numericUpDown_Money_5.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_5.Name = "numericUpDown_Money_5";
            this.numericUpDown_Money_5.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_5.TabIndex = 11;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label7.Location = new System.Drawing.Point(5, 114);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(81, 26);
            this.label7.TabIndex = 12;
            this.label7.Text = "Step 7";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label8.Location = new System.Drawing.Point(94, 114);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(82, 26);
            this.label8.TabIndex = 13;
            this.label8.Text = "Step 8";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label9.Location = new System.Drawing.Point(184, 114);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(83, 26);
            this.label9.TabIndex = 14;
            this.label9.Text = "Step 9";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_Money_6
            // 
            this.numericUpDown_Money_6.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_6.Location = new System.Drawing.Point(5, 145);
            this.numericUpDown_Money_6.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_6.Name = "numericUpDown_Money_6";
            this.numericUpDown_Money_6.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_6.TabIndex = 15;
            // 
            // numericUpDown_Money_7
            // 
            this.numericUpDown_Money_7.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_7.Location = new System.Drawing.Point(94, 145);
            this.numericUpDown_Money_7.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_7.Name = "numericUpDown_Money_7";
            this.numericUpDown_Money_7.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_7.TabIndex = 16;
            // 
            // numericUpDown_Money_8
            // 
            this.numericUpDown_Money_8.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_8.Location = new System.Drawing.Point(184, 145);
            this.numericUpDown_Money_8.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_8.Name = "numericUpDown_Money_8";
            this.numericUpDown_Money_8.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_8.TabIndex = 17;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label10.Location = new System.Drawing.Point(5, 170);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(81, 26);
            this.label10.TabIndex = 18;
            this.label10.Text = "Step 10";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label11.Location = new System.Drawing.Point(94, 170);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 26);
            this.label11.TabIndex = 19;
            this.label11.Text = "Step 11";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label12.Location = new System.Drawing.Point(184, 170);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(83, 26);
            this.label12.TabIndex = 20;
            this.label12.Text = "Step 12";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label13.Location = new System.Drawing.Point(5, 226);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(81, 26);
            this.label13.TabIndex = 21;
            this.label13.Text = "Step 13";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label14.Location = new System.Drawing.Point(94, 226);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(82, 26);
            this.label14.TabIndex = 22;
            this.label14.Text = "Step 14";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("굴림", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label15.Location = new System.Drawing.Point(184, 226);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 26);
            this.label15.TabIndex = 23;
            this.label15.Text = "Step 15";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // numericUpDown_Money_10
            // 
            this.numericUpDown_Money_10.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_10.Location = new System.Drawing.Point(94, 201);
            this.numericUpDown_Money_10.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_10.Name = "numericUpDown_Money_10";
            this.numericUpDown_Money_10.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_10.TabIndex = 24;
            // 
            // numericUpDown_Money_11
            // 
            this.numericUpDown_Money_11.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_11.Location = new System.Drawing.Point(184, 201);
            this.numericUpDown_Money_11.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_11.Name = "numericUpDown_Money_11";
            this.numericUpDown_Money_11.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_11.TabIndex = 25;
            // 
            // numericUpDown_Money_9
            // 
            this.numericUpDown_Money_9.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_9.Location = new System.Drawing.Point(5, 201);
            this.numericUpDown_Money_9.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_9.Name = "numericUpDown_Money_9";
            this.numericUpDown_Money_9.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_9.TabIndex = 26;
            // 
            // numericUpDown_Money_14
            // 
            this.numericUpDown_Money_14.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_14.Location = new System.Drawing.Point(184, 257);
            this.numericUpDown_Money_14.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_14.Name = "numericUpDown_Money_14";
            this.numericUpDown_Money_14.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_14.TabIndex = 27;
            // 
            // numericUpDown_Money_13
            // 
            this.numericUpDown_Money_13.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_13.Location = new System.Drawing.Point(94, 257);
            this.numericUpDown_Money_13.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_13.Name = "numericUpDown_Money_13";
            this.numericUpDown_Money_13.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_13.TabIndex = 28;
            // 
            // numericUpDown_Money_12
            // 
            this.numericUpDown_Money_12.Increment = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numericUpDown_Money_12.Location = new System.Drawing.Point(5, 257);
            this.numericUpDown_Money_12.Maximum = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            this.numericUpDown_Money_12.Name = "numericUpDown_Money_12";
            this.numericUpDown_Money_12.Size = new System.Drawing.Size(81, 21);
            this.numericUpDown_Money_12.TabIndex = 29;
            // 
            // MoneyOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "MoneyOrder";
            this.Size = new System.Drawing.Size(272, 283);
            this.Load += new System.EventHandler(this.MoneyOrder_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_0)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_11)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_9)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_14)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_13)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_Money_12)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_8;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_7;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_5;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_4;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_3;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_2;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_0;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_12;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_13;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_14;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_9;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_10;
        private System.Windows.Forms.NumericUpDown numericUpDown_Money_11;
    }
}
