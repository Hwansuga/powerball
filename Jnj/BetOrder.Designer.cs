﻿namespace Jnj
{
    partial class BetOrder
    {
        /// <summary> 
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region 구성 요소 디자이너에서 생성한 코드

        /// <summary> 
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.comboBox_Order7 = new System.Windows.Forms.ComboBox();
            this.comboBox_Order6 = new System.Windows.Forms.ComboBox();
            this.comboBox_Order5 = new System.Windows.Forms.ComboBox();
            this.comboBox_Order4 = new System.Windows.Forms.ComboBox();
            this.comboBox_Order3 = new System.Windows.Forms.ComboBox();
            this.comboBox_Order2 = new System.Windows.Forms.ComboBox();
            this.comboBox_Order1 = new System.Windows.Forms.ComboBox();
            this.comboBox_Order0 = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlLight;
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(93, 26);
            this.panel1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("굴림", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.Location = new System.Drawing.Point(17, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order7, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order6, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order5, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order3, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.comboBox_Order0, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 26);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 12.5F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(93, 226);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // comboBox_Order7
            // 
            this.comboBox_Order7.DisplayMember = "파워볼 홀";
            this.comboBox_Order7.FormattingEnabled = true;
            this.comboBox_Order7.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order7.Location = new System.Drawing.Point(3, 199);
            this.comboBox_Order7.Name = "comboBox_Order7";
            this.comboBox_Order7.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order7.TabIndex = 7;
            this.comboBox_Order7.ValueMember = "파워볼 홀";
            // 
            // comboBox_Order6
            // 
            this.comboBox_Order6.DisplayMember = "파워볼 홀";
            this.comboBox_Order6.FormattingEnabled = true;
            this.comboBox_Order6.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order6.Location = new System.Drawing.Point(3, 171);
            this.comboBox_Order6.Name = "comboBox_Order6";
            this.comboBox_Order6.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order6.TabIndex = 6;
            this.comboBox_Order6.ValueMember = "파워볼 홀";
            // 
            // comboBox_Order5
            // 
            this.comboBox_Order5.DisplayMember = "파워볼 홀";
            this.comboBox_Order5.FormattingEnabled = true;
            this.comboBox_Order5.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order5.Location = new System.Drawing.Point(3, 143);
            this.comboBox_Order5.Name = "comboBox_Order5";
            this.comboBox_Order5.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order5.TabIndex = 5;
            this.comboBox_Order5.ValueMember = "파워볼 홀";
            // 
            // comboBox_Order4
            // 
            this.comboBox_Order4.DisplayMember = "파워볼 홀";
            this.comboBox_Order4.FormattingEnabled = true;
            this.comboBox_Order4.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order4.Location = new System.Drawing.Point(3, 115);
            this.comboBox_Order4.Name = "comboBox_Order4";
            this.comboBox_Order4.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order4.TabIndex = 4;
            this.comboBox_Order4.ValueMember = "파워볼 홀";
            // 
            // comboBox_Order3
            // 
            this.comboBox_Order3.DisplayMember = "파워볼 홀";
            this.comboBox_Order3.FormattingEnabled = true;
            this.comboBox_Order3.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order3.Location = new System.Drawing.Point(3, 87);
            this.comboBox_Order3.Name = "comboBox_Order3";
            this.comboBox_Order3.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order3.TabIndex = 3;
            this.comboBox_Order3.ValueMember = "파워볼 홀";
            // 
            // comboBox_Order2
            // 
            this.comboBox_Order2.DisplayMember = "파워볼 홀";
            this.comboBox_Order2.FormattingEnabled = true;
            this.comboBox_Order2.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order2.Location = new System.Drawing.Point(3, 59);
            this.comboBox_Order2.Name = "comboBox_Order2";
            this.comboBox_Order2.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order2.TabIndex = 2;
            this.comboBox_Order2.ValueMember = "파워볼 홀";
            // 
            // comboBox_Order1
            // 
            this.comboBox_Order1.DisplayMember = "파워볼 홀";
            this.comboBox_Order1.FormattingEnabled = true;
            this.comboBox_Order1.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order1.Location = new System.Drawing.Point(3, 31);
            this.comboBox_Order1.Name = "comboBox_Order1";
            this.comboBox_Order1.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order1.TabIndex = 1;
            this.comboBox_Order1.ValueMember = "파워볼 홀";
            // 
            // comboBox_Order0
            // 
            this.comboBox_Order0.DisplayMember = "파워볼 홀";
            this.comboBox_Order0.FormattingEnabled = true;
            this.comboBox_Order0.Items.AddRange(new object[] {
            "파워볼 홀",
            "파워볼 짝",
            "파워볼 오버",
            "파워볼 언더",
            "일반볼 홀",
            "일반볼 짝",
            "일반볼 오버",
            "일반볼 언더"});
            this.comboBox_Order0.Location = new System.Drawing.Point(3, 3);
            this.comboBox_Order0.Name = "comboBox_Order0";
            this.comboBox_Order0.Size = new System.Drawing.Size(87, 20);
            this.comboBox_Order0.TabIndex = 0;
            this.comboBox_Order0.ValueMember = "파워볼 홀";
            // 
            // BetOrder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.Name = "BetOrder";
            this.Size = new System.Drawing.Size(93, 252);
            this.Load += new System.EventHandler(this.BetOrder_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.ComboBox comboBox_Order7;
        private System.Windows.Forms.ComboBox comboBox_Order6;
        private System.Windows.Forms.ComboBox comboBox_Order5;
        private System.Windows.Forms.ComboBox comboBox_Order4;
        private System.Windows.Forms.ComboBox comboBox_Order3;
        private System.Windows.Forms.ComboBox comboBox_Order2;
        private System.Windows.Forms.ComboBox comboBox_Order1;
        private System.Windows.Forms.ComboBox comboBox_Order0;
    }
}
