﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;


public class GameController
{
    public PageJnj widget;
    public Room_STATE roomState = Room_STATE.Room_None;
    public string round = "";

    public Dictionary<string, string> dicBetHistory = new Dictionary<string, string>();
    public Dictionary<string, double> dicBetMoneyHistory = new Dictionary<string, double>();
    public Dictionary<string, string> dicRetHistory = new Dictionary<string, string>();

    public int betNum = 0;
    public int betStep = 0;

    public GameController(PageJnj widget_)
    {
        this.widget = widget_;
    }

    public void UpdateInfo()
    {
        this.roomState = widget.GetGameState();
        this.round = widget.GetRound();
    }

    public void Process_State()
    {
        MethodInfo method = GetType().GetMethod(roomState.ToString(), BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.FlattenHierarchy);
        if (method == null)
            return;

        method.Invoke(this, null);
    }

    public void Room_BetTime()
    {
        string curKey = this.round;
        if (dicBetHistory.Keys.Contains(curKey))
            return;

    }

    public void Room_Result()
    {
        string curKey = this.round;

        if (dicBetHistory.Keys.Contains(curKey)
            && dicRetHistory.Keys.Contains(curKey) == false)
        {

        }
    }

    public string PrintRoomState()
    {
        if (this.roomState == Room_STATE.Room_BetTime)
            return "배팅타임";
        else if (this.roomState == Room_STATE.Room_Result)
            return "결과 산출";
        else if (this.roomState == Room_STATE.Room_Wait)
            return "결과 대기";
        else
            return "임시 상태";
    }

    public void InitData()
    {
        dicBetHistory.Clear();
        dicBetMoneyHistory.Clear();
        dicRetHistory.Clear();

        betNum = 0;
        betStep = 0;
        roomState = Room_STATE.Room_None;
        round = "";
    }
}

