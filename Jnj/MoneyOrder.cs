﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;

namespace Jnj
{
    public partial class MoneyOrder : UserControl
    {
        List<double> saveVar = null;

        List<NumericUpDown> listNumeric = new List<NumericUpDown>();

        public MoneyOrder()
        {
            InitializeComponent();
        }

        private void MoneyOrder_Load(object sender, EventArgs e)
        {
            List<FieldInfo> listField = this.GetType().GetFields(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).ToList();
            List<FieldInfo> numerics = listField.FindAll(x => x.FieldType == typeof(NumericUpDown));
            Util.SortByNameNymber(numerics);
            foreach (FieldInfo item in numerics)
            {
                NumericUpDown temp = (NumericUpDown)item.GetValue(this);
                temp.Click += numericUpDown_Money_ValueChanged;
                temp.Leave += numericUpDown_Money_ValueChanged;

                listNumeric.Add(temp);
            }
        }

        public void ConnectVariable(ref List<double> listinfo_)
        {
            this.saveVar = listinfo_;

            this.UpdateUI();
        }

        public void UpdateUI()
        {
            if (this.saveVar == null)
                return;

            for (int i = 0; i < this.saveVar.Count; ++i)
            {
                if (i < listNumeric.Count)
                {
                    listNumeric[i].Value = (int)this.saveVar[i];
                }
            }

        }

        protected void numericUpDown_Money_ValueChanged(object sender, EventArgs e)
        {
            if (this.saveVar == null)
            {
                return;
            }

            NumericUpDown numeric = (NumericUpDown)sender;
            int i = int.Parse(numeric.Name.Replace("numericUpDown_Money_", ""));
            this.saveVar[i] = double.Parse(numeric.Value.ToString());
        }
    }
}
