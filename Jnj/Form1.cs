﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Jnj
{
    public partial class Form1 : Form
    {
        PageJnj pageJnj = new PageJnj();

        GameController gameController = null;

        public Form1()
        {
            InitializeComponent();
        }

        public void PrintLog(string msg_, bool betLog_ = false)
        {
            listBox_Log.Invoke(new MethodInvoker(delegate ()
            {
                Util.AutoLineStringAdd(listBox_Log, msg_);
            }));
        }

        private void button_Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            Global.SetOderDefault();
            Global.SetBetmoneyDefault();
#if TEST
            Util.KillProcess("chromedriver");
#else
            Util.KillProcess("chrome");
#endif
            EventUtil.ConnectCustomEvent(this);

            this.betOrder1.ConnectVariable(ref Global.listOrder);
            this.moneyOrder1.ConnectVariable(ref Global.listBetMoney);

            gameController = new GameController(this.pageJnj);

        }

        private void button_CreatePage_Click(object sender, EventArgs e)
        {
            PrintLog("페이지 생성");
            pageJnj.CreatePage();
            button_CreatePage.Enabled = false;
            PrintLog("로그인 후 게임 페이지로 이동해주세요");
        }

        void WorkThread()
        {
            gameController.InitData();
            gameController.widget.SetRootFrame();

            this.Invoke(new MethodInvoker(delegate
            {
                this.label_CurOrder.Text = $"{gameController.betNum + 1}";
                this.label_CurStep.Text = $"{gameController.betStep + 1}";
                this.label_CurRound.Text = $"";
                this.label_CurState.Text = $"";
            }));

            while(true)
            {
                if (Global.stop)
                    break;

                gameController.UpdateInfo();

                this.Invoke(new MethodInvoker(delegate
                {
                    this.label_CurRound.Text = gameController.round;
                    this.label_CurState.Text = gameController.PrintRoomState();
                }));

                gameController.Process_State();

                Thread.Sleep(1000);
            }

            PrintLog("정지 완료");

            this.Invoke(new MethodInvoker(delegate
            {
                betOrder1.Enabled = true;
                moneyOrder1.Enabled = true;
                tableLayoutPanel_Option.Enabled = true;
                button_Start.Enabled = true;

            }));  
        }

        private void button_Start_Click(object sender, EventArgs e)
        {

            PrintLog("시작");
            Global.stop = false;

            betOrder1.Enabled = false;
            moneyOrder1.Enabled = false;
            tableLayoutPanel_Option.Enabled = false;
            button_Start.Enabled = false;

            Task task = new Task(() => {
                this.WorkThread();
            });
            task.Start();

        }

        private void button_Stop_Click(object sender, EventArgs e)
        {
            PrintLog("정지 중...");
            button_Stop.Enabled = false;

            Global.stop = true;
        }
    }
}
