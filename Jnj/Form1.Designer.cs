﻿namespace Jnj
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button_Close = new System.Windows.Forms.Button();
            this.bunifuDragControl1 = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.listBox_Log = new System.Windows.Forms.ListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel_Option = new System.Windows.Forms.TableLayoutPanel();
            this.checkBox_Real = new System.Windows.Forms.CheckBox();
            this.button_Stop = new System.Windows.Forms.Button();
            this.button_Start = new System.Windows.Forms.Button();
            this.button_CreatePage = new System.Windows.Forms.Button();
            this.panel_GameState = new System.Windows.Forms.Panel();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_CurStep = new System.Windows.Forms.Label();
            this.label_CurOrder = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.moneyOrder1 = new Jnj.MoneyOrder();
            this.betOrder1 = new Jnj.BetOrder();
            this.label_CurRound = new System.Windows.Forms.Label();
            this.label_CurState = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel_Option.SuspendLayout();
            this.panel_GameState.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(76)))), ((int)(((byte)(75)))), ((int)(((byte)(105)))));
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.button_Close);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(478, 27);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Image = global::Jnj.Properties.Resources.Logo1;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(53, 27);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // button_Close
            // 
            this.button_Close.Dock = System.Windows.Forms.DockStyle.Right;
            this.button_Close.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.button_Close.Location = new System.Drawing.Point(452, 0);
            this.button_Close.Name = "button_Close";
            this.button_Close.Size = new System.Drawing.Size(26, 27);
            this.button_Close.TabIndex = 0;
            this.button_Close.Text = "X";
            this.button_Close.UseVisualStyleBackColor = true;
            this.button_Close.Click += new System.EventHandler(this.button_Close_Click);
            // 
            // bunifuDragControl1
            // 
            this.bunifuDragControl1.Fixed = true;
            this.bunifuDragControl1.Horizontal = true;
            this.bunifuDragControl1.TargetControl = this.panel1;
            this.bunifuDragControl1.Vertical = true;
            // 
            // listBox_Log
            // 
            this.listBox_Log.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.listBox_Log.FormattingEnabled = true;
            this.listBox_Log.ItemHeight = 12;
            this.listBox_Log.Location = new System.Drawing.Point(0, 348);
            this.listBox_Log.Name = "listBox_Log";
            this.listBox_Log.Size = new System.Drawing.Size(478, 184);
            this.listBox_Log.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(50)))), ((int)(((byte)(49)))), ((int)(((byte)(69)))));
            this.panel2.Controls.Add(this.tableLayoutPanel_Option);
            this.panel2.Controls.Add(this.button_Stop);
            this.panel2.Controls.Add(this.button_Start);
            this.panel2.Controls.Add(this.button_CreatePage);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(111, 321);
            this.panel2.TabIndex = 2;
            // 
            // tableLayoutPanel_Option
            // 
            this.tableLayoutPanel_Option.ColumnCount = 1;
            this.tableLayoutPanel_Option.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel_Option.Controls.Add(this.checkBox_Real, 0, 2);
            this.tableLayoutPanel_Option.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.tableLayoutPanel_Option.Location = new System.Drawing.Point(0, 245);
            this.tableLayoutPanel_Option.Name = "tableLayoutPanel_Option";
            this.tableLayoutPanel_Option.RowCount = 3;
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Option.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel_Option.Size = new System.Drawing.Size(111, 76);
            this.tableLayoutPanel_Option.TabIndex = 8;
            // 
            // checkBox_Real
            // 
            this.checkBox_Real.AutoSize = true;
            this.checkBox_Real.BackColor = System.Drawing.Color.Transparent;
            this.checkBox_Real.Dock = System.Windows.Forms.DockStyle.Fill;
            this.checkBox_Real.ForeColor = System.Drawing.SystemColors.AppWorkspace;
            this.checkBox_Real.Location = new System.Drawing.Point(3, 53);
            this.checkBox_Real.Name = "checkBox_Real";
            this.checkBox_Real.Size = new System.Drawing.Size(105, 20);
            this.checkBox_Real.TabIndex = 0;
            this.checkBox_Real.Text = "Real";
            this.checkBox_Real.UseVisualStyleBackColor = false;
            // 
            // button_Stop
            // 
            this.button_Stop.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Stop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Stop.ForeColor = System.Drawing.Color.White;
            this.button_Stop.Location = new System.Drawing.Point(0, 46);
            this.button_Stop.Name = "button_Stop";
            this.button_Stop.Size = new System.Drawing.Size(111, 23);
            this.button_Stop.TabIndex = 7;
            this.button_Stop.Text = "정지";
            this.button_Stop.UseVisualStyleBackColor = true;
            this.button_Stop.Click += new System.EventHandler(this.button_Stop_Click);
            // 
            // button_Start
            // 
            this.button_Start.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_Start.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_Start.ForeColor = System.Drawing.Color.White;
            this.button_Start.Location = new System.Drawing.Point(0, 23);
            this.button_Start.Name = "button_Start";
            this.button_Start.Size = new System.Drawing.Size(111, 23);
            this.button_Start.TabIndex = 6;
            this.button_Start.Text = "시작";
            this.button_Start.UseVisualStyleBackColor = true;
            this.button_Start.Click += new System.EventHandler(this.button_Start_Click);
            // 
            // button_CreatePage
            // 
            this.button_CreatePage.Dock = System.Windows.Forms.DockStyle.Top;
            this.button_CreatePage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_CreatePage.ForeColor = System.Drawing.Color.White;
            this.button_CreatePage.Location = new System.Drawing.Point(0, 0);
            this.button_CreatePage.Name = "button_CreatePage";
            this.button_CreatePage.Size = new System.Drawing.Size(111, 23);
            this.button_CreatePage.TabIndex = 5;
            this.button_CreatePage.Text = "페이지 생성";
            this.button_CreatePage.UseVisualStyleBackColor = true;
            this.button_CreatePage.Click += new System.EventHandler(this.button_CreatePage_Click);
            // 
            // panel_GameState
            // 
            this.panel_GameState.Controls.Add(this.label_CurState);
            this.panel_GameState.Controls.Add(this.label_CurRound);
            this.panel_GameState.Controls.Add(this.label4);
            this.panel_GameState.Controls.Add(this.label3);
            this.panel_GameState.Controls.Add(this.label_CurStep);
            this.panel_GameState.Controls.Add(this.label_CurOrder);
            this.panel_GameState.Controls.Add(this.label2);
            this.panel_GameState.Controls.Add(this.label1);
            this.panel_GameState.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel_GameState.Location = new System.Drawing.Point(111, 27);
            this.panel_GameState.Name = "panel_GameState";
            this.panel_GameState.Size = new System.Drawing.Size(367, 55);
            this.panel_GameState.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(204, 26);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(54, 20);
            this.label4.TabIndex = 5;
            this.label4.Text = "State :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label3.Location = new System.Drawing.Point(194, 3);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 20);
            this.label3.TabIndex = 4;
            this.label3.Text = "Round :";
            // 
            // label_CurStep
            // 
            this.label_CurStep.AutoSize = true;
            this.label_CurStep.BackColor = System.Drawing.Color.Transparent;
            this.label_CurStep.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_CurStep.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_CurStep.Location = new System.Drawing.Point(71, 26);
            this.label_CurStep.Name = "label_CurStep";
            this.label_CurStep.Size = new System.Drawing.Size(47, 20);
            this.label_CurStep.TabIndex = 3;
            this.label_CurStep.Text = "None";
            // 
            // label_CurOrder
            // 
            this.label_CurOrder.AutoSize = true;
            this.label_CurOrder.BackColor = System.Drawing.Color.Transparent;
            this.label_CurOrder.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_CurOrder.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_CurOrder.Location = new System.Drawing.Point(71, 3);
            this.label_CurOrder.Name = "label_CurOrder";
            this.label_CurOrder.Size = new System.Drawing.Size(47, 20);
            this.label_CurOrder.TabIndex = 2;
            this.label_CurOrder.Text = "None";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label2.Location = new System.Drawing.Point(16, 26);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(49, 20);
            this.label2.TabIndex = 1;
            this.label2.Text = "Step :";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label1.Location = new System.Drawing.Point(6, 3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(59, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Order :";
            // 
            // moneyOrder1
            // 
            this.moneyOrder1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.moneyOrder1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.moneyOrder1.Location = new System.Drawing.Point(204, 82);
            this.moneyOrder1.Name = "moneyOrder1";
            this.moneyOrder1.Size = new System.Drawing.Size(274, 266);
            this.moneyOrder1.TabIndex = 5;
            // 
            // betOrder1
            // 
            this.betOrder1.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.betOrder1.Dock = System.Windows.Forms.DockStyle.Left;
            this.betOrder1.Location = new System.Drawing.Point(111, 82);
            this.betOrder1.Name = "betOrder1";
            this.betOrder1.Size = new System.Drawing.Size(93, 266);
            this.betOrder1.TabIndex = 4;
            // 
            // label_CurRound
            // 
            this.label_CurRound.AutoSize = true;
            this.label_CurRound.BackColor = System.Drawing.Color.Transparent;
            this.label_CurRound.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_CurRound.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_CurRound.Location = new System.Drawing.Point(264, 3);
            this.label_CurRound.Name = "label_CurRound";
            this.label_CurRound.Size = new System.Drawing.Size(47, 20);
            this.label_CurRound.TabIndex = 6;
            this.label_CurRound.Text = "None";
            // 
            // label_CurState
            // 
            this.label_CurState.AutoSize = true;
            this.label_CurState.BackColor = System.Drawing.Color.Transparent;
            this.label_CurState.Font = new System.Drawing.Font("맑은 고딕", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(129)));
            this.label_CurState.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label_CurState.Location = new System.Drawing.Point(264, 26);
            this.label_CurState.Name = "label_CurState";
            this.label_CurState.Size = new System.Drawing.Size(47, 20);
            this.label_CurState.TabIndex = 7;
            this.label_CurState.Text = "None";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(44)))), ((int)(((byte)(43)))), ((int)(((byte)(60)))));
            this.ClientSize = new System.Drawing.Size(478, 532);
            this.Controls.Add(this.moneyOrder1);
            this.Controls.Add(this.betOrder1);
            this.Controls.Add(this.panel_GameState);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.listBox_Log);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Jnj";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel_Option.ResumeLayout(false);
            this.tableLayoutPanel_Option.PerformLayout();
            this.panel_GameState.ResumeLayout(false);
            this.panel_GameState.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuDragControl bunifuDragControl1;
        private System.Windows.Forms.ListBox listBox_Log;
        private System.Windows.Forms.Button button_Close;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button button_Stop;
        private System.Windows.Forms.Button button_Start;
        private System.Windows.Forms.Button button_CreatePage;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel_Option;
        private System.Windows.Forms.CheckBox checkBox_Real;
        private System.Windows.Forms.Panel panel_GameState;
        private BetOrder betOrder1;
        private MoneyOrder moneyOrder1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_CurStep;
        private System.Windows.Forms.Label label_CurOrder;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_CurState;
        private System.Windows.Forms.Label label_CurRound;
    }
}

