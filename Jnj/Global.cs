﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

public enum Room_STATE
{
    Room_None = -1,
    Room_BetTime = 0,
    Room_DealCard = 1,
    Room_Result = 2,
    Room_ChangeShoe = 3,
    Room_ChangeDealer = 4,
    Room_Wait = 5,
}

class Global : Singleton<Global>
{
    public static List<string> listOrder = new List<string>();
    public static List<double> listBetMoney = new List<double>();

    public static bool stop = false;

    public static void SetOderDefault()
    {
        for(int i=0; i<8; ++i)
        {
            listOrder.Add("파워볼 홀");
        }
    }

    public static void SetBetmoneyDefault()
    {
        for(int i=0; i<15; ++i)
        {
            listBetMoney.Add(1000);
        }
    }
}

