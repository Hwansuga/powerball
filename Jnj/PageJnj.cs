﻿using OpenQA.Selenium;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

public class PageJnj : DriverController
{
    string url = "http://www.jnj-888.com/users/login";

    IWebElement rootFrame = null;

    public void CreatePage()
    {
        CreatDriver(TYPE_DRIVER.TYPE_Chrome);

        driver.Navigate().GoToUrl(url);
        driver.Manage().Window.Size = new System.Drawing.Size(1280, 1000);

        Thread.Sleep(2000);
#if TEST
        Login();
#endif
    }

    public void SetRootFrame()
    {
        this.rootFrame = driver.FindElementById("live-iframe");
    }

    void Login()
    {
        
        driver.FindElementById("UserUsername").SendKeys("add5868");
        Thread.Sleep(500);
        driver.FindElementById("UserPassword").SendKeys("add5868@");
        Thread.Sleep(500);
    }

    public void SetBetMoney(int value_)
    {
        List<IWebElement> listBtn = driver.FindElementByXPath("/html/body/div[4]/div/div[2]/div[3]/ul").FindElements(By.XPath("./li")).ToList();
        IWebElement resetBtn = listBtn.Find(x => this.IsContainAsHtml(x , "취소"));
        resetBtn.Click();
        Thread.Sleep(1000);

        IWebElement web = driver.FindElementByXPath("/html/body/div[4]/div/div[2]/div[2]/input");
        web.SendKeys(value_.ToString());
        Thread.Sleep(1000);

    }

    public void ClickBetSide(string value_)
    {
        IWebElement root = driver.FindElementByXPath("/html/body/div[4]/div/div[1]/div/div[5]/div");
        List<IWebElement> listBtn = root.FindElements(By.XPath("//*[@data-game-code='PWB']")).ToList();
        IWebElement btn = listBtn.Find(x => x.Text.Contains(value_));
        string innerHtml = btn.GetAttribute("class");

        if (innerHtml.Contains("pick-btn-selected") == false)
            ClickElement(btn);

        Thread.Sleep(1000);
    }

    public Room_STATE GetGameState()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);

        IWebElement root = driver.FindElementByXPath("/html/body/div[5]/div/div[1]/div[3]/div/div[2]");
        if (this.IsContainAsHtml(root , "none"))
        {
            IWebElement ret = driver.FindElementById("btm_res_box");
            string innerHTML = ret.GetAttribute("innerHTML");
            int ballCnt = Regex.Matches(innerHTML, "balance_game").Count;
            if (ballCnt >= 6)
            {
                return Room_STATE.Room_Result;
            }
            else
            {
                return Room_STATE.Room_Wait;
            }           
        }
        else
        {
            int remainTime = this.GetRemainTime();
            if (remainTime >= 30)
            {
                return Room_STATE.Room_BetTime;
            }
            else
            {
                return Room_STATE.Room_Wait;
            }
        }
    }

    public List<int> GetResultBall()
    {
        driver.SwitchTo().DefaultContent();
        driver.SwitchTo().Frame(rootFrame);

        IWebElement ballRet = driver.FindElementByXPath("/html/body/div[5]/div/div[1]/div[3]/div/div[1]");
        List<IWebElement> listBall = ballRet.FindElements(By.XPath("./p/img")).ToList();

        List<int> ret = new List<int>();
        listBall.ForEach(elem => {
            ret.Add(int.Parse(elem.GetAttribute("alt")));
        });

        return ret;
    }

    public int GetRemainTime()
    {
        try
        {
            IWebElement root = driver.FindElementByClassName("power_before_info");
            string stringMin = root.FindElement(By.Id("r_mm")).Text;
            string stringSec = root.FindElement(By.Id("r_ss")).Text;
            int min = int.Parse(stringMin);
            int sec = int.Parse(stringSec);
            return min * 60 + sec;
        }
        catch
        {
            return -1;
        }      
    }

    public string GetRound()
    {
        IWebElement root = driver.FindElementById("btm_curr_num");
        return root.Text;
    }
}

